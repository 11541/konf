package pl.bogdanski.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.bogdanski.dao.entity.Lecture;


@Repository
public interface LectureRepo extends CrudRepository<Lecture, Long> {
}

