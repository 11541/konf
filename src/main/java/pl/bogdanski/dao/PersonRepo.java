package pl.bogdanski.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.bogdanski.dao.entity.Person;

import java.util.Optional;

@Repository
public interface PersonRepo extends CrudRepository<Person, Long> {
    Optional<Person> findByLogin(String login);
}
