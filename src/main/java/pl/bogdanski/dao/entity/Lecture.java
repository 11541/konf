package pl.bogdanski.dao.entity;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.List;

@Entity
public class Lecture {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    private String name;
    private LocalTime startDate;
    private LocalTime endDate;
    private String path;
    private Integer nrParticipants;

    public Lecture() {
    }

    public Lecture(Long id, String name, LocalTime startDate, LocalTime endDate, String path, Integer nrParticipants) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.path = path;
        this.nrParticipants = nrParticipants;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalTime startDate) {
        this.startDate = startDate;
    }

    public LocalTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalTime endDate) {
        this.endDate = endDate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getNrParticipants() {
        return nrParticipants;
    }

    public void setNrParticipants(Integer nrParticipants) {
        this.nrParticipants = nrParticipants;
    }

    @ManyToMany(mappedBy = "lectures")
    private List<Person> persons;
}

