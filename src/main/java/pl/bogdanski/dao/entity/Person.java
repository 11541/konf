package pl.bogdanski.dao.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Person {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    private String login;
    private String email;

    public Person() {
    }

    public Person(Long id, String login, String email) {
        this.id = id;
        this.login = login;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "person_lecture",
    joinColumns = @JoinColumn(name = "lecture_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "person_id",
    referencedColumnName = "id"))
    private List<Lecture> lectures;
}
