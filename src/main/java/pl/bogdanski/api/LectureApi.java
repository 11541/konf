package pl.bogdanski.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.bogdanski.dao.entity.Lecture;
import pl.bogdanski.manager.LectureManager;

@RestController
@RequestMapping("/api/lecture")
public class LectureApi {
    private LectureManager lectureManager;

    @Autowired
    public LectureApi(LectureManager lectureManager) {
        this.lectureManager = lectureManager;
    }

    @GetMapping("/all")
    public Iterable<Lecture> getAll() {
        return lectureManager.findAll();

    }
}
