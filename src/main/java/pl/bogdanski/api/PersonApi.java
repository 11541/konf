package pl.bogdanski.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.bogdanski.dao.entity.Person;
import pl.bogdanski.manager.PersonManager;
import java.util.Optional;

@RestController
@RequestMapping("/api/person")
public class PersonApi {
    private PersonManager personManager;

    @Autowired
    public PersonApi(PersonManager personManager) {
        this.personManager = personManager;
    }

    @GetMapping("/all")
    public Iterable<Person> getAll(){
        return personManager.findAll();
    }

    @GetMapping
    public Optional<Person> getById(@RequestParam Long id){
        return personManager.findById(id);
    }
    @GetMapping("/login")
    public Optional<Person> getByLogin(@RequestParam String login) {
        return personManager.findByLogin(login);
    }
    @PostMapping
    public Person addPerson(@RequestBody Person person){
        return personManager.save(person);
    }
    @PatchMapping
    public Person updatePerson(@RequestBody Person person){
        return personManager.save(person);
    }
}

