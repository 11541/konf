package pl.bogdanski.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bogdanski.dao.PersonRepo;
import pl.bogdanski.dao.entity.Person;

import java.util.Optional;

@Service
public class PersonManager {
    private PersonRepo personRepo;

    @Autowired
    public PersonManager(PersonRepo personRepo) {
        this.personRepo = personRepo;
    }

    public Optional<Person> findById(Long id){
        return personRepo.findById(id);
    }

    public Iterable<Person> findAll(){
        return personRepo.findAll();
    }

    public Person save(Person person) {
        return personRepo.save(person);
    }

    public void deleteById(Long id) {
        personRepo.deleteById(id);
    }

    public Optional<Person> findByLogin(String login){
        return personRepo.findByLogin(login);
    }

}
