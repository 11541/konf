package pl.bogdanski.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.bogdanski.dao.LectureRepo;
import pl.bogdanski.dao.entity.Lecture;

import java.time.LocalTime;

@Service
public class LectureManager {
    private LectureRepo lectureRepo;

    @Autowired
    public LectureManager(LectureRepo lectureRepo) {
        this.lectureRepo = lectureRepo;
    }

    public Iterable<Lecture> findAll(){
        return lectureRepo.findAll();
    }

    public Lecture save(Lecture lecture){
        return lectureRepo.save(lecture);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        save(new Lecture(1L, "Język Programowania 1", LocalTime.of(10,0),LocalTime.of(11,45), "Ściezka A", 0));
        save(new Lecture(2L, "Język Programowania 2", LocalTime.of(12,0),LocalTime.of(13,45), "Ściezka A", 0));
        save(new Lecture(3L, "Język Programowania 3", LocalTime.of(14,0),LocalTime.of(15,45), "Ściezka A", 0));
        save(new Lecture(4L, "Bazy Danych - MySql", LocalTime.of(10,0),LocalTime.of(11,45), "Ściezka B", 0));
        save(new Lecture(5L, "Bazy Danych - MongoDB", LocalTime.of(12,0),LocalTime.of(13,45), "Ściezka B", 0));
        save(new Lecture(6L, "Bazy Danych - H2", LocalTime.of(14,0),LocalTime.of(15,45), "Ściezka B", 0));
        save(new Lecture(7L, "Sieci Komputerowe", LocalTime.of(10,0),LocalTime.of(11,45), "Ściezka C", 0));
        save(new Lecture(8L, "Bezpieczenstwo w Sieci", LocalTime.of(12,0),LocalTime.of(13,45), "Ściezka C", 0));
        save(new Lecture(9L, "Prawne Aspekty Informatyki", LocalTime.of(14,0),LocalTime.of(15,45), "Ściezka C", 0));
    }
}
